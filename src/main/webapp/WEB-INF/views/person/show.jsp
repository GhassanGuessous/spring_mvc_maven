<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Person</title>
</head>
<body>
<form>
    <div class="form-group">
        <label for="cin">CIN</label>
        <input type="text" class="form-control" id="cin" value="${person.cin}" disabled="disabled">
    </div>
    <div class="form-group">
        <label for="nom">NOM</label>
        <input type="text" class="form-control" id="nom" value="${person.nom}" disabled="disabled">
    </div>
    <div class="form-group">
        <label for="prenom">PRENOM</label>
        <input type="text" class="form-control" id="prenom" value="${person.prenom}" disabled="disabled">
    </div>
</form>
</body>
</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create</title>
</head>
<body>
<form method="POST" action="/persons/add" modelAttribute="person">
    <div class="form-group">
        <label for="cin">CIN</label>
        <input type="text" class="form-control" id="cin" placeholder="CIN">
    </div>
    <div class="form-group">
        <label for="nom">NOM</label>
        <input type="text" class="form-control" id="nom" placeholder="NOM">
    </div>
    <div class="form-group">
        <label for="prenom">PRENOM</label>
        <input type="text" class="form-control" id="prenom" placeholder="PRENOM">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</body>
</html>
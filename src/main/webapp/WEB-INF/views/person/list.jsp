<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>List</title>
</head>
<body>
<table class="table">
    <thead>
    <tr>
        <th scope="col">CIN</th>
        <th scope="col">NOM</th>
        <th scope="col">PRENOM</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${persons}" var="person">
    <tr>
        <th scope="row">1</th>
        <td>${person.cin}</td>
        <td>${person.nom}</td>
        <td>${person.prenom}</td>
    </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
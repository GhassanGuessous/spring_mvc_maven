package com.guess.service;

import com.guess.entities.Person;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService {

    private List<Person> persons = new ArrayList<Person>();

    public void create(Person person) {
        persons.add(person);
    }

    public List<Person> findAll() {
        return persons;
    }

    public Person findById(int id) {
        String cin = "" + id;
        for (Person person: persons
             ) {
            if(person.getCin() == cin) return person;
        }
        return null;
    }
}

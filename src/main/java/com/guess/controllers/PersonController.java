package com.guess.controllers;

import com.guess.service.PersonService;
import com.guess.entities.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/persons")
public class PersonController {

    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/add-form", method = RequestMethod.GET)
    public String addForm(){
        return "/person/create";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@ModelAttribute("person") Person person){
        personService.create(person);
        return "redirect:all";
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public String getList(Model model){
        model.addAttribute("persons", personService.findAll());
        return "/person/list";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getSignlePerson(@PathVariable int id, Model model){
        model.addAttribute("person", personService.findById(id));
        return "/person/show";
    }
}
